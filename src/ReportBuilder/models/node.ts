import { INode, ReportSortDirectionType } from 'peekdata-datagateway-api-sdk';

export interface IDimension extends INode { }

export interface IMetric extends INode { }

export interface ISelectedNode {
  value?: string;
  sorting?: ReportSortDirectionType;
}

export function isNode(node: string | INode): node is INode {
  if (!node) {
    return false;
  }

  return (node as INode).name !== undefined;
}
