import React from "react";

interface IProps {
  button: any;
  loader: React.ReactNode;
  loading: boolean;
}

class CustomExportButton extends React.PureComponent<IProps, {}> {
  public render() {
    const { loading, loader, button } = this.props;
    return (
      <div className="rb-custom-export-button-container">
        {loading ? loader : button}
      </div>
    );
  }
}

export default CustomExportButton;
