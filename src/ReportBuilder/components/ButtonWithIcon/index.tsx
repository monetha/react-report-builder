import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classnames from 'classnames';
import React from 'react';
import 'src/style/components/tooltip.scss';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IProps {
  title: string;
  styleClasses: string;
  icon: IconProp;
  disabled?: boolean;
  onClick?: () => void;
  upgradePlanOptions?: {
    setBanner: (title: string) => React.ReactElement;
    onMouseEnter: (event: React.MouseEvent<HTMLAnchorElement>) => void;
    onMouseLeave: (event: React.MouseEvent<HTMLAnchorElement>) => void;
  }
}

// #endregion

// #region -------------- Component -------------------------------------------------------------------

export class ButtonWithIcon extends React.PureComponent<IProps> {
  public render() {
    const { styleClasses, disabled, icon, title, upgradePlanOptions } = this.props;

    return (
      <>
        {upgradePlanOptions && upgradePlanOptions.setBanner(title)}
        <a className={
          classnames({
            btn: true,
            disabled,
          }, styleClasses)}
          onClick={this.onClick}
          onMouseEnter={upgradePlanOptions && upgradePlanOptions.onMouseEnter}
          onMouseLeave={upgradePlanOptions && upgradePlanOptions.onMouseLeave}
        >
          <i><FontAwesomeIcon icon={icon} /></i>
          {title}
        </a>
      </>
    );
  }

  private onClick = () => {
    const { disabled, onClick } = this.props;

    if (onClick && !disabled) {
      onClick();
    }
  }
}

// #endregion
