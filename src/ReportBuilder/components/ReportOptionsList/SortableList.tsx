import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { ReportColumnType } from 'peekdata-datagateway-api-sdk';
import React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
import { ButtonWithIcon } from 'src/ReportBuilder/components/ButtonWithIcon';
import { ISelectedNode } from 'src/ReportBuilder/models/node';
import { ITranslations } from 'src/ReportBuilder/models/translations';
import { ISortableItemProps } from './DragHandle';
import { SortableItem } from './SortableItem';
import { SortButton } from './SortButton';
import Tooltip from 'react-tooltip-lite';
import { hasPermission } from 'src/ReportBuilder/utils/tooltipUtils';

// #region -------------- Interfaces -------------------------------------------------------------------

export interface ISortableListProps extends ISortableItemProps {
  selectedOptions: ISelectedNode[];
  buttonTitle: string;
  listTitle: string;
  showButton?: boolean;
  isOptional?: boolean;
  t: ITranslations;
  onOptionAdded: (payload: ReportColumnType) => void;
  upgradePlanOptions?: {
    setBanner: (title: string) => React.ReactElement;
    onMouseEnter: (event: React.MouseEvent<HTMLAnchorElement>) => void;
    onMouseLeave: (event: React.MouseEvent<HTMLAnchorElement>) => void;
  }
}
interface IState {
  tipState: boolean;
}
// #endregion

// #region -------------- Component -------------------------------------------------------------------

class SortableList extends React.PureComponent<ISortableListProps, IState> {
  public constructor(props) {
    super(props);

    this.state = { tipState: false };
    this.bodyClick = this.bodyClick.bind(this);
  }

  public tipHandler = tipState => {
    this.setState({ tipState });
  }

  public componentDidMount() {
    document.addEventListener('mousedown', this.bodyClick);
  }

  public componentWillUnmount() {
    document.removeEventListener('mousedown', this.bodyClick);
  }

  public bodyClick(e) {
    if (e.target.className === 'link-to-general-page') {
      return;
    }

    this.setState({ tipState: false });
  }

  public render() {
    const { tipState } = this.state;
    const { options, selectedOptions, optionType, buttonTitle, showButton, listTitle, isOptional, onOptionAdded, t, upgradePlanOptions, ...otherProps } = this.props;

    return (
      <div>
        <div className='rb-title-dark rb-title-extra-small'>{listTitle} {isOptional && <span>- {t.optionalLabel}</span>}</div>
        <div className='rb-report-options'>

          {selectedOptions.map((selectedOption, index) => {
            const disabled = !!(selectedOption && selectedOption.value);
            let filteredOptions = options;

            if (!disabled) {
              const selectedValues = selectedOptions.map(selected => selected.value);

              filteredOptions = options.filter(option => selectedValues.indexOf(option.name) === -1);
            }

            return (
              <SortableItem
                key={index}
                index={index}
                disabled={!disabled}
                options={[...filteredOptions]}
                optionType={optionType}
                selectedOption={selectedOption}
                selectDisabled={disabled}
                sortButton={<SortButton sorting={selectedOption.sorting} />}
                {...otherProps}
              />
            );
          })}

          {showButton && <div className='rb-btn-container'>
              <Tooltip
                  content={[t.tooltipTextOnAddOptionButtons]}
                  isOpen={tipState}
                  className='custom-tooltip-message'
                  distance={15}
                  padding='15px'
                  zIndex={99999}
              >
                <ButtonWithIcon
                  title={buttonTitle}
                  styleClasses='rb-btn-small rb-btn-caramel-dashed'
                  icon={faPlusCircle}
                  disabled={!hasPermission}
                  onClick={() => onOptionAdded(optionType)}
                  upgradePlanOptions={upgradePlanOptions}
                />
              </Tooltip>
          </div>}
        </div>
      </div>
    );
  }
}

// #endregion

const withSortableContainer = SortableContainer(SortableList);

export { withSortableContainer as SortableList };
