import Collapse from "@kunukn/react-collapse";
import cx from "classnames";
import {
    INotOptimizedReportResponse,
    IOptimizedReportResponse,
    IReportRequest,
    PeekdataApi,
    ReportColumnType
} from "peekdata-datagateway-api-sdk";
import React, { Fragment, ReactNode } from "react";
import { connect } from "react-redux";
import { FilterControls } from "src/ReportBuilder/components/FilterControls";
import { ReportOptionsList } from "src/ReportBuilder/components/ReportOptionsList";
import { ReportTabs } from "src/ReportBuilder/components/ReportTabs";
import { RowsLimitInput } from "src/ReportBuilder/components/RowsLimitInput";
import { Spinner } from "src/ReportBuilder/components/Spinner";
import { ViewDropDowns } from "src/ReportBuilder/components/ViewDropDowns";
import {
    defaultRows,
    setRowsDefaultLimit,
    setRowsDefaultOffset
} from "src/ReportBuilder/constants/rows";
import {
    IDimension,
    IMetric,
    ISelectedNode
} from "src/ReportBuilder/models/node";
import { ITranslations } from "src/ReportBuilder/models/translations";
import { setPeekdataApi } from "src/ReportBuilder/services/api";
import { IAsyncState } from "src/ReportBuilder/state/action";
import {
    addDataModel,
    changeLimitRowsTo,
    changeStartWithRow,
    expandReportOptions,
    generateReportRequest,
    ILoadNodesPayloadRequest, IReportOptionsPayload, ISelectNodePayload,
    ISortNodePayload,
    ISortOrderNodePayload,
    loadDataModelNames,
    loadDataModelNodes,
    loadReportRequest,
    selectDataModel,
    setTranslations,
    sortEnd,
    sortOrder,
    unselectDataModel
} from "src/ReportBuilder/state/actions";
import { IReportBuilderState } from "src/ReportBuilder/state/reducers";
import { ICompatibilityState } from "src/ReportBuilder/state/reducers/compatibility";
import { IReportOptionsState } from "src/ReportBuilder/state/reducers/reportOptions";
import { IRgb } from "src/ReportBuilder/utils/Color";
import { setPermissionToUseBuilder } from 'src/ReportBuilder/utils/tooltipUtils';
import { DataModelDropDown } from "../DataModelDropDown";
// #region -------------- Interfaces -------------------------------------------------------------------

interface IStateProps {
    dimensions: IAsyncState<IDimension[]>;
    metrics: IAsyncState<IMetric[]>;
    selectedDimensions: ISelectedNode[];
    selectedMetrics: ISelectedNode[];
    compatibility: ICompatibilityState;
    limitRowsTo: number;
    startWithRow: number;
    request: IReportRequest;
    scopeNames: IAsyncState<string[]>;
    selectedScope: string;
    dataModelNames: IAsyncState<string[]>;
    selectedDataModel: string;
    t: ITranslations;
    reportOptions: IReportOptionsState;
    dataFull: IAsyncState<INotOptimizedReportResponse>;
    dataOptimized: IAsyncState<IOptimizedReportResponse>;
    file: IAsyncState<string>;
    select: IAsyncState<string>;
}

interface IDispatchProps {
    onOptionAdded: (payload: ReportColumnType) => void;
    onOptionSelected: (payload: ISelectNodePayload) => void;
    onOptionUnselected: (payload: ISelectNodePayload) => void;
    onSortOrder: (payload: ISortOrderNodePayload) => void;
    onSortEnd: (payload: ISortNodePayload) => void;
    onChangeLimitRowsTo: (payload: number) => void;
    onChangeStartWithRow: (payload: number) => void;
    onScopeChanged: (scope: string) => void;
    onDataModelChanged: (payload: ILoadNodesPayloadRequest) => void;
    onLoadReportRequest: (request: Partial<IReportRequest>) => void;
    onGenerateReportRequest: () => void;
    setTranslations: (translations: Partial<ITranslations>) => void;
    onReportOptionsChange: (payload: IReportOptionsPayload) => void;
}

interface IDefaultProps {
    loader: ReactNode;
    showScopesDropdown: boolean;
    showDataModelDropdown: boolean;
    showDimensionsList: boolean;
    showMetricsList: boolean;
    showFilters: boolean;
    showRowsOffset: boolean;
    showRowsLimit: boolean;
    defaultRowsOffset: number;
    defaultRowsLimit: number;
    maxRowsLimit: number;
    showRequestViewButton: boolean;
    showResponseViewButton: boolean;
    showDataTabs: boolean;
    showChart: boolean;
    showDataTable: boolean;
    defaultTab: number;
    chartColors?: IRgb[];
    hasPermissionToUseBuilder?: boolean;
}

export interface IReportBuilderProps extends Partial<IDefaultProps> {
    peekdataApi: PeekdataApi;
    translations?: Partial<ITranslations>;
    reportRequest?: Partial<IReportRequest>;
    getSelectedResources?: (props: IProps) => any;
    customExportButton?: any;
    isReportOptionsOpen?: boolean;
    upgradePlanOptions?: {
      setBanner: (title: string) => React.ReactElement;
      onMouseEnter: (event: React.MouseEvent<HTMLAnchorElement>) => void;
      onMouseLeave: (event: React.MouseEvent<HTMLAnchorElement>) => void;
    }
}

export interface IProps
    extends IStateProps,
    IDispatchProps,
    IReportBuilderProps { }

// #endregion

// #region -------------- Component -------------------------------------------------------------------

class ReportBuilder extends React.PureComponent<IProps> {
    public static defaultProps: IDefaultProps = {
        loader: <Spinner />,
        showScopesDropdown: true,
        showDataModelDropdown: true,
        showDimensionsList: true,
        showMetricsList: true,
        showFilters: true,
        showRowsOffset: true,
        showRowsLimit: true,
        defaultRowsOffset: defaultRows.offset,
        defaultRowsLimit: defaultRows.limit,
        maxRowsLimit: 10000,
        showRequestViewButton: true,
        showResponseViewButton: true,
        showDataTabs: true,
        showChart: true,
        showDataTable: true,
        defaultTab: 0,
        hasPermissionToUseBuilder: true,
    };

    public constructor(props: IProps) {
        super(props);

        const {
            peekdataApi,
            translations,
            defaultRowsOffset,
            defaultRowsLimit,
            setTranslations,
            hasPermissionToUseBuilder,
        } = this.props;

        setPeekdataApi(peekdataApi);
        setTranslations(translations);
        setRowsDefaultOffset(defaultRowsOffset);
        setRowsDefaultLimit(defaultRowsLimit);
        setPermissionToUseBuilder(hasPermissionToUseBuilder);
    }

    public componentDidMount() {
        const {
            reportRequest,
            onLoadReportRequest,
            onChangeStartWithRow,
            onChangeLimitRowsTo,
            defaultRowsOffset,
            defaultRowsLimit,
            onReportOptionsChange,
            isReportOptionsOpen
        } = this.props;
        onChangeStartWithRow(defaultRowsOffset);
        onChangeLimitRowsTo(defaultRowsLimit);
        onLoadReportRequest(reportRequest);
        onReportOptionsChange({ isReportOptionsOpen })
    }

    public componentDidUpdate(prevProps: IProps) {
        const {
            translations,
            peekdataApi,
            onLoadReportRequest,
            reportRequest,
            onChangeStartWithRow,
            onChangeLimitRowsTo,
            defaultRowsOffset,
            defaultRowsLimit,
            setTranslations,
            selectedDimensions,
            selectedMetrics,
            startWithRow,
            limitRowsTo,
            request,
            getSelectedResources
        } = this.props;
        const prevTranslations = prevProps && prevProps.translations;
        const prevPeekdataApi = prevProps && prevProps.peekdataApi;
        const prevReportRequest = prevProps && prevProps.reportRequest;
        const prevRowsOffset = prevProps && prevProps.defaultRowsOffset;
        const prevRowsLimit = prevProps && prevProps.defaultRowsLimit;
        const prevSelectedDimensions =
            prevProps && prevProps.selectedDimensions;
        const prevSelectedMetrics = prevProps && prevProps.selectedMetrics;
        const prevStartWithRow = prevProps && prevProps.startWithRow;
        const prevLimitRowsTo = prevProps && prevProps.limitRowsTo;
        const prevDateRanges =
            prevProps && prevProps.request?.filters?.dateRanges;
        const prevSingleKeys =
            prevProps && prevProps.request?.filters?.singleKeys;
        const prevSingleValues =
            prevProps && prevProps.request?.filters?.singleValues;

        if (prevRowsOffset !== defaultRowsOffset) {
            onChangeStartWithRow(defaultRowsOffset);
        }

        if (prevRowsLimit !== defaultRowsLimit) {
            onChangeLimitRowsTo(defaultRowsLimit);
        }

        if (prevTranslations !== translations) {
            setTranslations(translations);
        }

        if (prevPeekdataApi !== peekdataApi) {
            setPeekdataApi(peekdataApi);
        }

        if (
            JSON.stringify(prevReportRequest) !== JSON.stringify(reportRequest)
        ) {
            onLoadReportRequest(reportRequest);
        }

        if (
            prevSelectedDimensions !== selectedDimensions ||
            prevSelectedMetrics !== selectedMetrics ||
            prevStartWithRow !== startWithRow ||
            prevLimitRowsTo !== limitRowsTo ||
            prevDateRanges !== request?.filters?.dateRanges ||
            prevSingleKeys !== request?.filters?.singleKeys ||
            prevSingleValues !== request?.filters?.singleValues ||
            JSON.stringify(prevReportRequest) !== JSON.stringify(reportRequest)
        ) {
            getSelectedResources?.(this.props);
        }
    }

    public render() {
        return (
            <div className='rb-report-builder-container'>
                {this.renderError()}
                {this.renderDataModelDropDowns()}

                <div className='rb-report-container'>
                    {this.renderLoader()}
                    {this.renderReportBuilderContent()}
                </div>
            </div>
        );
    }

    // #region -------------- DataModel dropdowns -------------------------------------------------------------------

    private renderDataModelDropDowns = () => {
        const {
            scopeNames,
            dataModelNames,
            selectedScope,
            selectedDataModel,
            onScopeChanged,
            onDataModelChanged,
            showScopesDropdown,
            showDataModelDropdown,
            t
        } = this.props;

        return (
            <DataModelDropDown
                scopeNames={scopeNames}
                dataModelNames={dataModelNames}
                selectedScope={selectedScope}
                selectedDataModel={selectedDataModel}
                showScopesDropdown={showScopesDropdown}
                showDataModelsDropdown={showDataModelDropdown}
                onScopeChanged={onScopeChanged}
                onDataModelChanged={onDataModelChanged}
                t={t}
            />
        );
    };

    // #endregion

    // #region -------------- Loader -------------------------------------------------------------------

    private renderLoader = () => {
        const { loader } = this.props;

        if (!this.isLoading()) {
            return null;
        }

        return loader;
    };

    private isLoading = () => {
        const { dimensions, metrics } = this.props;

        return (
            (dimensions && dimensions.isFetching) ||
            (metrics && metrics.isFetching)
        );
    };

    // #endregion

    // #region -------------- Errors -------------------------------------------------------------------

    private renderError = () => {
        const e = this.getErrors();
        if (!e || e.length === 0) {
            return null;
        }

        const text = e.join("\n");

        return <div className='alert alert-danger'>{text}</div>;
    };

    private getErrors(): string[] {
        const e = new Set<string>();

        const { dimensions, metrics, compatibility, t } = this.props;

        const dimensionsError = dimensions && dimensions.error;
        if (dimensionsError) {
            e.add(dimensionsError);
        }

        const metricsError = metrics && metrics.error;
        if (metricsError) {
            e.add(metricsError);
        }

        const { scopeNames, dataModelNames: datamodelNames } = this.props;
        if (
            (scopeNames && scopeNames.error) ||
            (datamodelNames && datamodelNames.error)
        ) {
            e.add(scopeNames.error || datamodelNames.error);
        }

        if (compatibility && compatibility.error) {
            e.add(compatibility.error);
        }

        const { dataFull, dataOptimized, file, select } = this.props;
        if (
            (dataFull && dataFull.error) ||
            (file && file.error) ||
            (select && select.error)
        ) {
            e.add(
                dataFull.error ||
                file.error ||
                select.error
            );
        }

        if (dataOptimized && dataOptimized.error) {
            e.clear();
            e.add(t.incompatibleDimensionError);
        }

        return Array.from(e);
    }

    // #endregion

    // #region -------------- Report builder content -------------------------------------------------------------------

    private renderReportBuilderContent = () => {
        const { selectedDataModel, onReportOptionsChange, reportOptions, t } =
            this.props;

        if (!selectedDataModel) {
            return null;
        }

        return (
            <Fragment>
                <div className='rb-report-content'>
                    <div
                        className='rb-report-header'
                        onClick={() => onReportOptionsChange({
                            isReportOptionsOpen: !reportOptions.isReportOptionsOpen
                        })}>
                        <span className='rotate90'>
                            <svg
                                className={cx("icon", {
                                    "icon--expanded":
                                        reportOptions.isReportOptionsOpen
                                })}
                                viewBox='6 0 12 24'>
                                <polygon points='8 0 6 1.8 14.4 12 6 22.2 8 24 18 12' />
                            </svg>
                        </span>
                        <span className='rb-title-dark rb-title-small'>
                            {t.contentTitle}
                        </span>
                    </div>

                    <Collapse
                        className='rb-report-option-content'
                        overflowOnExpanded={true}
                        isOpen={reportOptions.isReportOptionsOpen}>
                        {this.renderMetricsList()}
                        {this.renderDimensionsList()}
                        {this.renderRowsLimit()}
                    </Collapse>
                </div>
                {this.renderFilters()}
                {this.renderViewDropDowns()}
                {this.renderTabs()}
            </Fragment>
        );
    };

    // #endregion

    // #endregion

    // #region -------------- Dimensions/Metrics lists -------------------------------------------------------------------

    private renderDimensionsList = () => {
        const {
            dimensions,
            selectedDimensions,
            onOptionAdded,
            onOptionSelected,
            onOptionUnselected,
            onSortEnd,
            showDimensionsList,
            t,
            upgradePlanOptions
        } = this.props;

        if (
            !showDimensionsList ||
            !dimensions ||
            !dimensions.data ||
            dimensions.data.length === 0
        ) {
            return null;
        }

        return (
            <ReportOptionsList
                options={dimensions.data}
                selectedOptions={selectedDimensions}
                optionType={ReportColumnType.dimension}
                listTitle={t.dimensionsListTitle}
                placeholder={t.dimensionPlaceholder}
                noResultsText={t.noDimensionsText}
                buttonTitle={t.addDimensionButtonText}
                onOptionAdded={onOptionAdded}
                onOptionSelected={onOptionSelected}
                onOptionUnselected={onOptionUnselected}
                onSortOrder={this.onSortOrder}
                onSortEnd={onSortEnd}
                isOptional={true}
                t={t}
                upgradePlanOptions={upgradePlanOptions}
            />
        );
    };

    private renderMetricsList = () => {
        const {
            metrics,
            selectedMetrics,
            onOptionAdded,
            onOptionSelected,
            onOptionUnselected,
            onSortEnd,
            showMetricsList,
            t,
            upgradePlanOptions
        } = this.props;

        if (
            !showMetricsList ||
            !metrics ||
            !metrics.data ||
            metrics.data.length === 0
        ) {
            return null;
        }

        return (
            <ReportOptionsList
                options={metrics.data}
                selectedOptions={selectedMetrics}
                optionType={ReportColumnType.metric}
                listTitle={t.metricsListTitle}
                placeholder={t.metricPlaceholder}
                noResultsText={t.noMetricsText}
                buttonTitle={t.addMetricButtonText}
                onOptionAdded={onOptionAdded}
                onOptionSelected={onOptionSelected}
                onOptionUnselected={onOptionUnselected}
                onSortOrder={this.onSortOrder}
                onSortEnd={onSortEnd}
                t={t}
                upgradePlanOptions={upgradePlanOptions}
            />
        );
    };

    private onSortOrder = (payload: ISortOrderNodePayload) => {
        const { onSortOrder, onGenerateReportRequest } = this.props;

        onSortOrder(payload);
        onGenerateReportRequest();
    };

    // #endregion

    // #region -------------- Filters -------------------------------------------------------------------

    private renderFilters = () => {
        const { showFilters, upgradePlanOptions } = this.props;

        if (!showFilters) {
            return null;
        }

        return <FilterControls upgradePlanOptions={upgradePlanOptions} />;
    };

    // #endregion

    // #region -------------- Rows limit -------------------------------------------------------------------

    private renderRowsLimit = () => {
        const {
            limitRowsTo,
            startWithRow,
            showRowsOffset,
            showRowsLimit,
            maxRowsLimit,
            t
        } = this.props;

        return (
            <RowsLimitInput
                startWithRow={startWithRow}
                limitRowsTo={limitRowsTo}
                showRowsOffset={showRowsOffset}
                showRowsLimit={showRowsLimit}
                onStartWithRowChanged={this.onChangeStartWithRow}
                onLimitRowsToChanged={this.onChangeLimitRowsTo}
                maxRowsLimit={maxRowsLimit}
                t={t}
            />
        );
    };

    private onChangeStartWithRow = (rowsOffset: number) => {
        const { onChangeStartWithRow, onGenerateReportRequest } = this.props;

        onChangeStartWithRow(rowsOffset);
        onGenerateReportRequest();
    };

    private onChangeLimitRowsTo = (rowsLimit: number) => {
        const { onChangeLimitRowsTo, onGenerateReportRequest } = this.props;

        onChangeLimitRowsTo(rowsLimit);
        onGenerateReportRequest();
    };

    // #endregion

    // #region -------------- View dropdowns -------------------------------------------------------------------

    private renderViewDropDowns = () => {
        const {
            showRequestViewButton,
            showResponseViewButton,
            customExportButton,
            loader
        } = this.props;

        if (!this.showRequestResults()) {
            return null;
        }

        return (
            <ViewDropDowns
                showRequestViewButton={showRequestViewButton}
                showResponseViewButton={showResponseViewButton}
                customExportButton={customExportButton}
                loader={loader}
            />
        );
    };

    private showRequestResults = () => {
        const { request } = this.props;

        return request && request.metrics && request.metrics.length > 0;
    };

    // #endregion

    // #region -------------- Tabs -------------------------------------------------------------------

    private renderTabs = () => {
        const {
            showDataTabs,
            showChart,
            showDataTable,
            defaultTab,
            loader,
            chartColors
        } = this.props;

        if (!this.showRequestResults()) {
            return null;
        }

        return (
            <ReportTabs
                showDataTabs={showDataTabs}
                showChart={showChart}
                showDataTable={showDataTable}
                defaultTab={defaultTab}
                loader={loader}
                chartColors={chartColors}
            />
        );
    };

    // #endregion
}

// #endregion

// #region -------------- Connect -------------------------------------------------------------------

const connected = connect<
    IStateProps,
    IDispatchProps,
    IReportBuilderProps,
    IReportBuilderState
>(
    (state) => {
        const {
            dimensions,
            metrics,
            selectedDimensions,
            selectedMetrics,
            compatibility,
            limitRowsTo,
            startWithRow,
            request,
            scopeNames,
            dataModelNames,
            translations,
            reportOptions,
            dataFull,
            dataOptimized,
            file,
            select
        } = state;

        return {
            dimensions,
            metrics,
            selectedDimensions,
            selectedMetrics,
            compatibility,
            limitRowsTo,
            startWithRow,
            request,
            scopeNames: scopeNames && scopeNames.scopeNames,
            selectedScope: scopeNames && scopeNames.selectedScope,
            dataModelNames: dataModelNames && dataModelNames.dataModelNames,
            selectedDataModel: dataModelNames && dataModelNames.selectedDataModel,
            t: translations,
            reportOptions,
            dataFull,
            dataOptimized,
            file,
            select
        };
    },
    (dispatch) => {
        return {
            onOptionAdded: (payload: ReportColumnType) =>
                dispatch(addDataModel(payload)),
            onOptionSelected: (payload: ISelectNodePayload) =>
                dispatch(selectDataModel(payload)),
            onOptionUnselected: (payload: ISelectNodePayload) =>
                dispatch(unselectDataModel(payload)),
            onSortOrder: (payload: ISortOrderNodePayload) =>
                dispatch(sortOrder(payload)),
            onSortEnd: (payload: ISortNodePayload) => dispatch(sortEnd(payload)),
            onChangeStartWithRow: (payload: number) =>
                dispatch(changeStartWithRow(payload)),
            onChangeLimitRowsTo: (payload: number) =>
                dispatch(changeLimitRowsTo(payload)),
            onScopeChanged: (scope: string) => dispatch(loadDataModelNames(scope)),
            onDataModelChanged: (payload: ILoadNodesPayloadRequest) =>
                dispatch(loadDataModelNodes(payload)),
            onLoadReportRequest: (reportRequest: Partial<IReportRequest>) =>
                dispatch(loadReportRequest(reportRequest)),
            onGenerateReportRequest: () => dispatch(generateReportRequest()),
            setTranslations: (translations: Partial<ITranslations>) =>
                dispatch(setTranslations(translations)),
            onReportOptionsChange: (payload: IReportOptionsPayload) =>
                dispatch(expandReportOptions(payload))
        };
    }
)(ReportBuilder);

// #endregion

export { connected as ReportBuilder };

