import { IOptimizedReportResponse } from "peekdata-datagateway-api-sdk";
import queryString from "query-string";
import React from "react";
import ReactTable, { Column } from "react-table";
import { ITranslations } from "src/ReportBuilder/models/translations";
import { formatIntervalUnits } from "src/ReportBuilder/utils/DateUtils";
import "src/style/components/table.scss";

// #region -------------- Interfaces -------------------------------------------------------------------

interface IProps {
  data: IOptimizedReportResponse;
  t: ITranslations;
}

interface ITableQueryParams {
  rows: number;
  page: number;
}

// #endregion

const initialTableOptions: ITableQueryParams = {
  rows: 20,
  page: 1,
};

// #region -------------- Component -------------------------------------------------------------------

export class ReportTable extends React.Component<IProps> {
  state = {
    parsedQueryParams: initialTableOptions,
  };
  componentDidMount() {
    const { rows, page } = parseQuery();
    if (
      rows !== this.state.parsedQueryParams?.rows ||
      page !== this.state.parsedQueryParams?.page
    ) {
      this.setState({ parsedQueryParams: { rows, page } }, () => {
        this.setQueryString();
      });
    }
  }
  componentDidUpdate(_, prevState) {
    if (
      prevState.parsedQueryParams?.rows !==
        this.state.parsedQueryParams?.rows ||
      prevState?.parsedQueryParams?.page !== this.state.parsedQueryParams?.page
    ) {
      this.setQueryString();
    }
  }

  private setQueryString = () => {
    const url = new URL(window.location.href);
    url.searchParams.set("page", this.state.parsedQueryParams.page.toString());
    url.searchParams.set("rows", this.state.parsedQueryParams.rows.toString());
    window.history.pushState("RowsAndPage", "UpdatedQueryString", url.search);
  };

  public render() {
    const { data, t } = this.props;

    if (!data) {
      return null;
    }

    const columnHeaders = data && data.columnHeaders;
    const rows = data.rows;

    const tableData = [];
    if (rows && rows.length > 0) {
      for (const row of rows) {
        const dataRow = {};

        for (let i = 0; i < row.length; i += 1) {
          const adjustedRow: any = row;

          if (
            adjustedRow[i] &&
            typeof adjustedRow[i] === "object" &&
            adjustedRow[i]["type"] === "interval"
          ) {
            adjustedRow[i] = formatIntervalUnits(adjustedRow[i]);
          }

          if (
            typeof adjustedRow[i] === "string" ||
            typeof adjustedRow[i] === "number"
          ) {
            dataRow[columnHeaders[i].name] = adjustedRow[i];
          } else if (typeof adjustedRow[i] === "boolean") {
            dataRow[columnHeaders[i].name] = String(adjustedRow[i]);
          } else {
            dataRow[columnHeaders[i].name] = adjustedRow[i]?.value ?? "-";
          }
        }

        tableData.push(dataRow);
      }
    }

    const columns: Column[] = columnHeaders.map((header, i, columns) => ({
      Header: header.title,
      accessor: header.name,
      resizable: columns[i] !== columns[columns.length - 1], // prevent resize the last column
    }));

    return (
      <div className="rb-table">
        <ReactTable
          className="-striped"
          data={tableData}
          columns={columns}
          previousText={t.tablePreviousText}
          nextText={t.tableNextText}
          loadingText={t.tableLoadingText}
          noDataText={t.tableNoDataText}
          pageText={t.tablePageText}
          ofText={t.tableOfText}
          rowsText={t.tableRowsText}
          pageJumpText={t.tablePageJumpText}
          rowsSelectorText={t.tableRowsSelectorText}
          pageSize={this.state.parsedQueryParams?.rows ?? 20}
          page={
            this.state.parsedQueryParams?.page
              ? this.state.parsedQueryParams?.page - 1
              : 1
          }
          onPageChange={(pageIndex) => {
            this.setState({
              parsedQueryParams: {
                page: pageIndex + 1,
                rows: this.state.parsedQueryParams.rows,
              },
            });
          }}
          onPageSizeChange={(pageSize, pageIndex) => {
            this.setState({
              parsedQueryParams: {
                page: pageIndex + 1,
                rows: pageSize,
              },
            });
          }}
        />
      </div>
    );
  }
}

// #endregion

//Parsing query string

const parseQuery = (): ITableQueryParams => {
  let rows = parseFloat(queryString.parse(location.search)?.rows);
  let page = parseFloat(queryString.parse(location.search)?.page);

  if (Number.isNaN(rows) || rows < 0 || rows % 1 !== 0) {
    rows = 20;
  }
  if (
    Number.isNaN(page) ||
    page < 0 ||
    page < 1 ||
    page % 1 !== 0 ||
    page > 100000
  ) {
    page = 1;
  }

  if (
    rows === 5 ||
    rows === 10 ||
    rows === 20 ||
    rows === 25 ||
    rows === 50 ||
    rows === 75 ||
    rows === 100
  ) {
    return {
      rows,
      page,
    };
  } else {
    return {
      rows: 20,
      page,
    };
  }
};
