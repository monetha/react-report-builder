import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { connect } from 'react-redux';
import Tooltip from 'react-tooltip-lite';
import { ButtonWithIcon } from 'src/ReportBuilder/components/ButtonWithIcon';
import { IFilter } from 'src/ReportBuilder/models/filter';
import { IDimension } from 'src/ReportBuilder/models/node';
import { ITranslations } from 'src/ReportBuilder/models/translations';
import { addFilter, changeFilterInput, generateReportRequest, IChangeFilterInput, ISelectFilter, removeFilter, selectFilterOption } from 'src/ReportBuilder/state/actions';
import { IReportBuilderState } from 'src/ReportBuilder/state/reducers';
import { hasPermission } from 'src/ReportBuilder/utils/tooltipUtils';
import { FilterControl } from './FilterControl';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IStateProps {
  filters: IFilter[];
  dimensions: IDimension[];
  t: ITranslations;
}

interface IDispatchProps {
  onFilterAddClicked: () => void;
  onFilterOptionSelected: (payload: ISelectFilter) => void;
  onFilterInputChanged: (payload: IChangeFilterInput) => void;
  onFilterRemoveClicked: (payload: IFilter) => void;
  onGenerateReportRequest: () => void;
}

interface IOwnProps { }

interface IProps extends IStateProps, IDispatchProps, IOwnProps {
  upgradePlanOptions?: {
    setBanner: (title: string) => React.ReactElement;
    onMouseEnter: (event: React.MouseEvent<HTMLAnchorElement>) => void;
    onMouseLeave: (event: React.MouseEvent<HTMLAnchorElement>) => void;
  }
}

interface IState {
  tipState: boolean;
}
// #endregion

// #region -------------- Component -------------------------------------------------------------------

class FilterControls extends React.PureComponent<IProps, IState> {
  public constructor(props) {
    super(props);

    this.state = { tipState: false };
    this.bodyClick = this.bodyClick.bind(this);
  }

  public tipHandler = tipState => {
    this.setState({ tipState });
  }

  public componentDidMount() {
    document.addEventListener('mousedown', this.bodyClick);
  }

  public componentWillUnmount() {
    document.removeEventListener('mousedown', this.bodyClick);
  }

  public bodyClick(e) {
    if (e.target.className === 'link-to-general-page') {
      return;
    }

    this.setState({ tipState: false });
  }

  public render() {
    const { tipState } = this.state;
    const { filters, dimensions, onFilterOptionSelected, onFilterInputChanged, onFilterAddClicked, onFilterRemoveClicked, onGenerateReportRequest, t, upgradePlanOptions } = this.props;

    return (
      <div className='rb-report-filters'>
        <div className='rb-title-dark rb-title-extra-small'>{t.filtersText}</div>

        {filters && filters.length !== 0 && <div className='rb-filter-container'>
          {filters.map((filter, index) =>
            <FilterControl
              key={index}
              filter={filter}
              dimensions={dimensions}
              onFilterRemoveClicked={onFilterRemoveClicked}
              onFilterOptionSelected={onFilterOptionSelected}
              onFilterInputChanged={onFilterInputChanged}
              onGenerateReportRequest={onGenerateReportRequest}
              upgradePlanOptions={upgradePlanOptions}
              t={t}
            />,
          )}
        </div>}

        <div className='rb-btn-container'>
          <Tooltip
              content={[t.tooltipTextOnAddOptionButtons]}
              isOpen={tipState}
              className='custom-tooltip-message'
              distance={15}
              padding='15px'
              zIndex={99999}
          >
            <ButtonWithIcon
              title={t.addFilterButton}
              styleClasses='rb-btn-small rb-btn-crimson'
              icon={faPlusCircle}
              disabled={!hasPermission}
              onClick={onFilterAddClicked}
              upgradePlanOptions={upgradePlanOptions}
            />
          </Tooltip>
        </div>
      </div>
    );
  }
}

// #endregion

// #region -------------- Connect -------------------------------------------------------------------

const connected = connect<IStateProps, IDispatchProps, IOwnProps, IReportBuilderState>(
  (state) => {
    const { filters, dimensions, translations } = state;

    return {
      filters,
      dimensions: dimensions && dimensions.data,
      t: translations,
    };
  },
  (dispatch) => {
    return {
      onFilterAddClicked: () => dispatch(addFilter()),
      onFilterOptionSelected: (payload: ISelectFilter) => dispatch(selectFilterOption(payload)),
      onFilterInputChanged: (payload: IChangeFilterInput) => dispatch(changeFilterInput(payload)),
      onFilterRemoveClicked: (payload: IFilter) => dispatch(removeFilter(payload)),
      onGenerateReportRequest: () => dispatch(generateReportRequest()),
    };
  },
)(FilterControls);

export { connected as FilterControls };

// #endregion
