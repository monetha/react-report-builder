export let hasPermission: boolean;

export function setPermissionToUseBuilder(permission: boolean) {
    hasPermission = permission;
}
