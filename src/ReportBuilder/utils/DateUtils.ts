import { round } from 'lodash';

export function formatIntervalUnits(interval: any, maxUnits: number = 2) {
  let formattedInterval = '';
  let unitCount = 0;
  if (interval) {
    if (interval.years) {
      formattedInterval += interval.years + ' years ';
      unitCount++;
    }
    if (interval.months) {
      formattedInterval += interval.months + ' months ';
      unitCount++;
    }
    if (unitCount < maxUnits && interval.days) {
      formattedInterval += interval.days + 'd ';
      unitCount++;
    }
    if (unitCount < maxUnits && interval.hours) {
      formattedInterval += interval.hours + 'hr ';
      unitCount++;
    }
    if (unitCount < maxUnits && interval.minutes) {
      formattedInterval += interval.minutes + 'min ';
      unitCount++;
    }
    if (unitCount < maxUnits && interval.seconds) {
      formattedInterval += round(interval.seconds) + ' seconds';
    }

    return formattedInterval.trimRight() || '-';
  }
  return '-';
}