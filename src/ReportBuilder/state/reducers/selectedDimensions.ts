import { ReportColumnType, ReportSortDirectionType } from 'peekdata-datagateway-api-sdk';
import { arrayMove } from 'react-sortable-hoc';
import { ISelectedNode } from 'src/ReportBuilder/models/node';
import { IAction } from 'src/ReportBuilder/state/action';
import { actionTypes, ISelectNodePayload, ISortNodePayload, ISortOrderNodePayload } from 'src/ReportBuilder/state/actions';
import { getSortedItems } from 'src/ReportBuilder/utils/SortingUtils';

// #region -------------- State -------------------------------------------------------------------

const initialState: ISelectedNode[] = [];

// #endregion

// #region -------------- Reducer -------------------------------------------------------------------

export function selectedDimensions(state: ISelectedNode[] = initialState, action: IAction): ISelectedNode[] {
  switch (action.type) {
    case actionTypes.loadScopeNames:
      if (action.payload) {
        return state;
      }

      return initialState;

    case actionTypes.dimensionsLoaded:
    case actionTypes.dataModelNamesLoaded:
      return initialState;

    case actionTypes.addOption:
      if (action.payload !== ReportColumnType.dimension) {
        return state;
      }

      return [
        ...state,
        {},
      ];

    case actionTypes.sortOrder:
      return getSortedItems(state, action.payload as ISortOrderNodePayload, ReportColumnType.dimension);

    case actionTypes.sortEnd:
      const { oldIndex, newIndex, optionType } = action.payload as ISortNodePayload;

      if (optionType !== ReportColumnType.dimension) {
        return state;
      }

      return arrayMove(state, oldIndex, newIndex);

    case actionTypes.setSelectedDimensions:
      return action.payload;

    case actionTypes.selectOption:
      const payload = action.payload as ISelectNodePayload;

      if (!payload || payload.optionType !== ReportColumnType.dimension) {
        return state;
      }

      return state.map((item, index) => {
        if (item.value) {
          return item;
        }

        let sorting = null;

        // First item has to have ASC sorting
        if (index === 0) {
          sorting = ReportSortDirectionType.ASC;
        }

        return {
          value: payload.value,
          sorting,
        };
      });

    case actionTypes.unselectOption:
      const { value: unselectedOptionValue, optionType: unselectOptionType } = action.payload as ISelectNodePayload;

      if (unselectOptionType !== ReportColumnType.dimension) {
        return state;
      }

      return state.filter(item => item.value !== unselectedOptionValue);

    default:
      return state;
  }
}

// #endregion
