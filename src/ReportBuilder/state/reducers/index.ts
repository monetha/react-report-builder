import { INotOptimizedReportResponse, IOptimizedReportResponse, IReportRequest } from 'peekdata-datagateway-api-sdk';
import { IFilter } from 'src/ReportBuilder/models/filter';
import { IDimension, IMetric, ISelectedNode } from 'src/ReportBuilder/models/node';
import { ITranslations } from 'src/ReportBuilder/models/translations';
import { IAsyncState } from 'src/ReportBuilder/state/action';
import { ICompatibilityState } from './compatibility';
import { IDataModelNamesState } from './dataModelNames';
import { IReportOptionsState } from './reportOptions';
import { IScopeNamesState } from './scopeNames';

// #region -------------- Interfaces -------------------------------------------------------------------

export interface IReportBuilderState {
  dataFull: IAsyncState<INotOptimizedReportResponse>;
  dataOptimized: IAsyncState<IOptimizedReportResponse>;
  dimensions: IAsyncState<IDimension[]>;
  compatibility: ICompatibilityState;
  file: IAsyncState<string>;
  filters: IFilter[];
  dataModelNames: IDataModelNamesState;
  limitRowsTo: number;
  startWithRow: number;
  metrics: IAsyncState<IMetric[]>;
  request: IReportRequest;
  scopeNames: IScopeNamesState;
  select: IAsyncState<string>;
  selectedDimensions: ISelectedNode[];
  selectedMetrics: ISelectedNode[];
  translations: ITranslations;
  reportOptions: IReportOptionsState;
}

// #endregion
